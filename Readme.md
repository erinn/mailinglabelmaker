# Mailing Label Printer

First cut, command-line only for now; GUI is coming!

This utility takes a spreadsheet of addresses, along with some configuration specifying the dimensions of your mailing labels, and outputs a PDF file with the labels pre-filled and ready to print.

This is designed to work with the CSV files output by Patreon's Benefits Tracker tool, but should in principle work with any properly formatted CSV file. Mailing labels are specified using the [Go template language](https://golang.org/pkg/text/template/) and the first row is used to determine column names; see the -template option below for an example.

Preview (using [fake data from Brian Dunning](https://www.briandunning.com/sample-data/)):

![mailing label preview](preview.png "mailing label preview")

# Usage

```
  -cols int
        number of columns (default 3)
  -csv string
        CSV file (downloaded from Patreon)
  -fontfamily string
        font family (default "Arial")
  -fontsize float
        font size (default 10)
  -fontstyle string
        font style (B for bold)
  -gapx float
        gap between columns (default 3.4)
  -gapy float
        gap between rows
  -h float
        label height (default 25.4)
  -lines
        draw lines?
  -o string
        filename to output PDF to (default "out.pdf")
  -paper string
        paper size (default "Letter")
  -portrait
        portrait mode? (default true)
  -row int
        number of rows (default 10)
  -skip int
        skip labels (useful for printing on partially-printed label sheets
  -template string
        mailing label template (default "{{.Addressee}}\n{{.Street}}\n{{.City}}, {{.State}}\n{{.Zip}}, {{.Country}}\n")
  -unitStr string
        units to use for other measurements (default "mm")
  -w float
        label width (default 66.8)
  -x float
        left margin (default 6.5)
  -y float
        top margin (default 12.7)
```

# Known Bugs

* Text that doesn't fit on one line isn't wrapped. For now, you can manually insert a linebreak in the relevant cell (using Calc or Excel or your preferred spreadsheet editor).
* unitStr doesn't affect fontsize
* In general nothing stops you from setting parameters that don't add up to the page dimensions
* Empty-address detection only works for Patreon CSVs