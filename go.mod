module git.openprivacy.ca/erinn/pdftest

go 1.12

require (
	git.openprivacy.ca/openprivacy/libricochet-go v1.0.8 // indirect
	github.com/jung-kurt/gofpdf v1.16.2
)
